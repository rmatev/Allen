velo_validator validation:
TrackChecker output                               :      2219/   257248   0.86% ghosts
01_velo                                           :    108923/   110735  98.36% ( 98.41%),      2339 (  2.10%) clones, pur  99.68%, hit eff  96.70%
02_long                                           :     62459/    62831  99.41% ( 99.45%),       984 (  1.55%) clones, pur  99.78%, hit eff  97.76%
03_long_P>5GeV                                    :     39426/    39563  99.65% ( 99.66%),       517 (  1.29%) clones, pur  99.81%, hit eff  98.29%
04_long_strange                                   :      2833/     2892  97.96% ( 98.44%),        43 (  1.50%) clones, pur  99.40%, hit eff  97.34%
05_long_strange_P>5GeV                            :      1318/     1341  98.28% ( 98.61%),        10 (  0.75%) clones, pur  99.31%, hit eff  98.63%
06_long_fromB                                     :      3904/     3937  99.16% ( 99.34%),        53 (  1.34%) clones, pur  99.67%, hit eff  97.87%
07_long_fromB_P>5GeV                              :      3236/     3252  99.51% ( 99.53%),        36 (  1.10%) clones, pur  99.71%, hit eff  98.26%
08_long_electrons                                 :      4604/     4744  97.05% ( 96.96%),       138 (  2.91%) clones, pur  98.02%, hit eff  96.68%
09_long_fromB_electrons                           :       182/      190  95.79% ( 96.06%),        11 (  5.70%) clones, pur  98.10%, hit eff  96.62%
10_long_fromB_electrons_P>5GeV                    :       122/      126  96.83% ( 97.44%),         9 (  6.87%) clones, pur  98.61%, hit eff  97.12%


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.928 (  4821/  5194)
Isolated             :  0.964 (  2597/  2694)
Close                :  0.890 (  2224/  2500)
False rate           :  0.014 (    69/  4890)
Real false rate      :  0.014 (    69/  4890)
Clones               :  0.000 (     0/  4821)


veloUT_validator validation:
TrackChecker output                               :      3635/    55744   6.52% ghosts
01_velo                                           :     49395/   110735  44.61% ( 44.96%),       432 (  0.87%) clones, pur  99.46%, hit eff  96.79%
02_velo+UT                                        :     49290/    96397  51.13% ( 51.50%),       431 (  0.87%) clones, pur  99.48%, hit eff  96.78%
03_velo+UT_P>5GeV                                 :     34510/    47679  72.38% ( 72.84%),       287 (  0.82%) clones, pur  99.59%, hit eff  97.65%
04_velo+notLong                                   :     10477/    47904  21.87% ( 21.96%),        86 (  0.81%) clones, pur  99.20%, hit eff  95.56%
05_velo+UT+notLong                                :     10386/    34396  30.20% ( 30.32%),        86 (  0.82%) clones, pur  99.28%, hit eff  95.54%
06_velo+UT+notLong_P>5GeV                         :      5424/     8812  61.55% ( 61.85%),        37 (  0.68%) clones, pur  99.51%, hit eff  97.63%
07_long                                           :     38918/    62831  61.94% ( 62.44%),       346 (  0.88%) clones, pur  99.53%, hit eff  97.12%
08_long_P>5GeV                                    :     29100/    39563  73.55% ( 74.04%),       251 (  0.86%) clones, pur  99.60%, hit eff  97.66%
09_long_fromB                                     :      3277/     3937  83.24% ( 85.06%),        26 (  0.79%) clones, pur  99.50%, hit eff  97.43%
10_long_fromB_P>5GeV                              :      2930/     3252  90.10% ( 90.95%),        24 (  0.81%) clones, pur  99.51%, hit eff  97.57%
11_long_electrons                                 :       837/     4744  17.64% ( 18.43%),        24 (  2.79%) clones, pur  97.97%, hit eff  95.16%
12_long_fromB_electrons                           :        79/      190  41.58% ( 46.96%),         4 (  4.82%) clones, pur  98.29%, hit eff  96.08%
13_long_fromB_electrons_P>5GeV                    :        73/      126  57.94% ( 64.90%),         4 (  5.19%) clones, pur  98.47%, hit eff  97.34%


forward_validator validation:
TrackChecker output                               :      1252/    36042   3.47% ghosts
for P>3GeV,Pt>0.5GeV                              :       657/    22587   2.91% ghosts
01_long                                           :     32935/    62831  52.42% ( 53.61%),       293 (  0.88%) clones, pur  99.15%, hit eff  95.57%
02_long_P>5GeV                                    :     27195/    39563  68.74% ( 69.70%),       233 (  0.85%) clones, pur  99.26%, hit eff  96.58%
03_long_strange                                   :      1011/     2892  34.96% ( 34.25%),         6 (  0.59%) clones, pur  98.69%, hit eff  94.90%
04_long_strange_P>5GeV                            :       742/     1341  55.33% ( 56.24%),         3 (  0.40%) clones, pur  98.77%, hit eff  96.19%
05_long_fromB                                     :      2985/     3937  75.82% ( 77.35%),        23 (  0.76%) clones, pur  99.24%, hit eff  96.73%
06_long_fromB_P>5GeV                              :      2785/     3252  85.64% ( 85.95%),        23 (  0.82%) clones, pur  99.28%, hit eff  97.06%
07_long_electrons                                 :       638/     4744  13.45% ( 14.25%),        20 (  3.04%) clones, pur  98.43%, hit eff  95.30%
08_long_electrons_P>5GeV                          :       559/     2357  23.72% ( 24.69%),        16 (  2.78%) clones, pur  98.61%, hit eff  96.25%
09_long_fromB_electrons                           :        65/      190  34.21% ( 39.66%),         3 (  4.41%) clones, pur  98.39%, hit eff  95.67%
10_long_fromB_electrons_P>5GeV                    :        63/      126  50.00% ( 56.73%),         3 (  4.55%) clones, pur  98.61%, hit eff  96.28%


muon_validator validation:
Muon fraction in all MCPs:                                              12755/   941546   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    415/    40275   0.01% 
Correctly identified muons with isMuon:                                   332/      415  80.00% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                      78/       92  84.78% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      3768/    39860   9.45% 
Ghost tracks identified as muon with isMuon:                              130/     1252  10.38% 


rate_validator validation:
Hlt1TwoTrackMVACharmXSec:      469/  1000, (14070.00 +/-   473.43) kHz
Hlt1KsToPiPi:                   27/  1000, (  810.00 +/-   153.77) kHz
Hlt1TrackMVA:                  226/  1000, ( 6780.00 +/-   396.78) kHz
Hlt1TwoTrackMVA:               441/  1000, (13230.00 +/-   471.03) kHz
Hlt1TwoTrackKs:                  4/  1000, (  120.00 +/-    59.88) kHz
Hlt1SingleHighPtMuon:            2/  1000, (   60.00 +/-    42.38) kHz
Hlt1SingleHighPtMuonNoMuID:      2/  1000, (   60.00 +/-    42.38) kHz
Hlt1LowPtMuon:                 114/  1000, ( 3420.00 +/-   301.50) kHz
Hlt1D2KK:                       19/  1000, (  570.00 +/-   129.52) kHz
Hlt1D2KPi:                      27/  1000, (  810.00 +/-   153.77) kHz
Hlt1D2PiPi:                     18/  1000, (  540.00 +/-   126.13) kHz
Hlt1DiMuonHighMass:             13/  1000, (  390.00 +/-   107.46) kHz
Hlt1DiMuonLowMass:              38/  1000, ( 1140.00 +/-   181.38) kHz
Hlt1DiMuonSoft:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowPtDiMuon:                55/  1000, ( 1650.00 +/-   216.28) kHz
Hlt1TrackMuonMVA:               12/  1000, (  360.00 +/-   103.30) kHz
Hlt1TrackElectronMVA:           34/  1000, ( 1020.00 +/-   171.93) kHz
Hlt1SingleHighPtElectron:        7/  1000, (  210.00 +/-    79.09) kHz
Hlt1DisplacedDielectron:         7/  1000, (  210.00 +/-    79.09) kHz
Hlt1DisplacedLeptons:           27/  1000, (  810.00 +/-   153.77) kHz
Hlt1SingleHighEt:                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:              1000/  1000, (30000.00 +/-     0.00) kHz
Hlt1GECPassthrough:            887/  1000, (26610.00 +/-   300.35) kHz
Hlt1NoBeam:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamOne:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamTwo:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BothBeams:                   1/  1000, (   30.00 +/-    29.98) kHz
Hlt1ODINLumi:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINNoBias:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:               1/  1000, (   30.00 +/-    29.98) kHz
Hlt1RICH1Alignment:              2/  1000, (   60.00 +/-    42.38) kHz
Hlt1RICH2Alignment:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                     0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                    1000/  1000, (30000.00 +/-     0.00) kHz


selreport_validator validation:
                            Events  Candidates
Hlt1TwoTrackMVACharmXSec:      469         902
Hlt1KsToPiPi:                   27          29
Hlt1TrackMVA:                  226         350
Hlt1TwoTrackMVA:               441        1251
Hlt1TwoTrackKs:                  4           4
Hlt1SingleHighPtMuon:            2           2
Hlt1SingleHighPtMuonNoMuID:      2           3
Hlt1LowPtMuon:                 114         131
Hlt1D2KK:                       19          21
Hlt1D2KPi:                      27          29
Hlt1D2PiPi:                     18          19
Hlt1DiMuonHighMass:             13          14
Hlt1DiMuonLowMass:              38          45
Hlt1DiMuonSoft:                  0           0
Hlt1LowPtDiMuon:                55          71
Hlt1TrackMuonMVA:               12          12
Hlt1TrackElectronMVA:           34          35
Hlt1SingleHighPtElectron:        7           7
Hlt1DisplacedDielectron:         7           7
Hlt1DisplacedLeptons:           27           0
Hlt1SingleHighEt:                0           0
Hlt1Passthrough:              1000           0
Hlt1GECPassthrough:            887           0
Hlt1NoBeam:                      0           0
Hlt1BeamOne:                     0           0
Hlt1BeamTwo:                     0           0
Hlt1BothBeams:                   1           0
Hlt1ODINLumi:                    0           0
Hlt1ODINNoBias:                  0           0
Hlt1VeloMicroBias:               1           0
Hlt1RICH1Alignment:              2           2
Hlt1RICH2Alignment:              0           0
Hlt1BeamGas:                     0           0

Total decisions: 3433
Total tracks:    2276
Total SVs:       1786
Total hits:      56511
Total stdinfo:   28785

